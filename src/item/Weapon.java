package item;

public class Weapon extends Item{
    private double damage;
    private double attackSpeed;
    private WeaponType type;

    public Weapon(String name, int requiredLevel, double damage, double attackSpeed, WeaponType type) {
        super(name, requiredLevel, Slot.WEAPON);
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.type = type;
    }

    public double getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public WeaponType getWeaponType() {
        return type;
    }

    public double getDPS() {
        return damage * attackSpeed;
    }
}
