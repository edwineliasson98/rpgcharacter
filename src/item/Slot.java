package item;

public enum Slot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
