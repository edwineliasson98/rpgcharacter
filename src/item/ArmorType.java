package item;

public enum ArmorType implements Type{
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
