package item;

public enum WeaponType implements Type {
    AXE,
    BOW,
    DAGGER,
    HAMMER,
    STAFF,
    SWORD,
    WAND,
}
