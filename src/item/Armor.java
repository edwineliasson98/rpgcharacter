package item;

import attributes.PrimaryAttributes;

public class Armor extends Item{
    private PrimaryAttributes attributes;
    private ArmorType type;

    public Armor(String name, int requiredLevel, Slot slot, ArmorType type, int strength, int dexterity, int intelligence) {
        super(name, requiredLevel, slot);
        this.type = type;
        this.attributes = new PrimaryAttributes(strength,dexterity,intelligence);
    }

    public ArmorType getArmorType() {
        return type;
    }

    public int[] getAttributes() {
        return attributes.getAttributes();
    }

    public int getStrength() {
        return attributes.getStrength();
    }

    public int getDexterity() {
        return attributes.getDexterity();
    }

    public int getIntelligence() {
        return attributes.getIntelligence();
    }
}
