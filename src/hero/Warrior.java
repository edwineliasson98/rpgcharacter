package hero;

import item.ArmorType;
import item.WeaponType;

public class Warrior extends Hero{
    // define starting values of attributes and valid weapon and armor types
    public Warrior(String name) {
        super(name,5,2,1,
                new WeaponType[] {WeaponType.AXE,WeaponType.HAMMER,WeaponType.SWORD},
                new ArmorType[] {ArmorType.MAIL,ArmorType.PLATE});
    }

    @Override
    public void increaseAttributes() {
        basePrimaryAttributes.increaseStrength(3);
        basePrimaryAttributes.increaseDexterity(2);
        basePrimaryAttributes.increaseIntelligence(1);
        totalPrimaryAttributes.increaseAttributes(new int[] {3,2,1});
    }

    @Override
    public int getTotalMainPrimaryAttribute() {
        return totalPrimaryAttributes.getStrength();
    }
}
