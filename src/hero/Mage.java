package hero;

import item.ArmorType;
import item.WeaponType;

public class Mage extends Hero{

    // define starting values of attributes and valid weapon and armor types
    public Mage(String name) {
        super(name,1,1,8,
                new WeaponType[] {WeaponType.WAND,WeaponType.STAFF},
                new ArmorType[] {ArmorType.CLOTH});
    }


    @Override
    public void increaseAttributes() {
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(1);
        basePrimaryAttributes.increaseIntelligence(5);
        totalPrimaryAttributes.increaseAttributes(new int[] {1,1,5});
    }

    @Override
    public int getTotalMainPrimaryAttribute() {
        return totalPrimaryAttributes.getIntelligence();
    }

}
