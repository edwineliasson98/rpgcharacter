package hero;

import attributes.PrimaryAttributes;
import item.*;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public abstract class Hero {
    private String name;
    private int level = 1;
    protected PrimaryAttributes basePrimaryAttributes;
    protected PrimaryAttributes totalPrimaryAttributes;
    private Map<Slot,Item> equipment = new HashMap<>();
    private WeaponType[] validWeapons;
    private ArmorType[] validArmors;

    // get name, attributes, and weapon and armor types from children
    public Hero(String name, int strength, int dexterity, int intelligence, WeaponType[] validWeapons, ArmorType[] validArmors) {
        this.name = name;
        this.basePrimaryAttributes = new PrimaryAttributes(strength,dexterity, intelligence);
        this.totalPrimaryAttributes = new PrimaryAttributes(basePrimaryAttributes);
        this.validWeapons = validWeapons;
        this.validArmors = validArmors;
    }

    //equip weapon
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        // check if valid level requirement && valid weapon type
        if (level < weapon.getRequiredLevel() || !checkValidItemType(weapon.getWeaponType(), validWeapons)) {
            throw new InvalidWeaponException("Invalid weapon, could not equip item.");
        }
        equipment.put(weapon.getSlot(), weapon);
        // equipping succeeded
        return true;
    }

    // equip armor
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        // check if valid level requirement && valid armor type
        if (level < armor.getRequiredLevel() || !checkValidItemType(armor.getArmorType(), validArmors)) {
            throw new InvalidArmorException("Invalid armor, could not equip item.");
        }
        // if slot as armor
        if (equipment.containsKey(armor.getSlot())) {
            // remove its attributes from total
            totalPrimaryAttributes.removeAttributes(((Armor)equipment.get(armor.getSlot())).getAttributes());
        }
        totalPrimaryAttributes.increaseAttributes(armor.getAttributes());
        equipment.put(armor.getSlot(),armor);
        // equipping succeeded
        return true;
    }

    public double getDPS() {
        double weaponDPS = 1;
        // check if weapon slot exist if so type cast and get its dps
        if(equipment.containsKey(Slot.WEAPON)) {
            weaponDPS = ((Weapon) equipment.get(Slot.WEAPON)).getDPS();
        }
        // return weapon dps * total main primary attribute
        return weaponDPS * (1 + (getTotalMainPrimaryAttribute() / 100.0));
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public void levelUp() {
        level += 1;
        increaseAttributes();
    }

    public int[] getBaseAttributes() {
        return basePrimaryAttributes.getAttributes();
    }

    public int getStrength() {
        return basePrimaryAttributes.getStrength();
    }

    public int getDexterity() {
        return basePrimaryAttributes.getDexterity();
    }

    public int getIntelligence() {
        return basePrimaryAttributes.getIntelligence();
    }

    // generic version of checking if weapon and armor types are valid
    private <T extends Type> boolean checkValidItemType(T itemType, T[] itemTypes) {
        // compare item type with valid types
        for (T type: itemTypes) {
            if (type == itemType) {
                return true;
            }
        }
        return false;
    }

    // abstract method for leveling up
    public abstract void increaseAttributes();

    // abstract method for getting main primary attribute
    public abstract int getTotalMainPrimaryAttribute();

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Name: " + name + "\n");
        result.append("Level: ").append(level).append("\n");
        result.append(totalPrimaryAttributes.toString()).append("\n");
        // loop through equipments
        if (!equipment.isEmpty()){
            result.append("Equipment: \n");
            for (Item item : equipment.values()) {
                result.append(item.getName()).append("\n");
            }
        }
        result.append("Hero DPS: ").append(getDPS()).append("\n");
        return result.toString();
    }
}
