package hero;

import item.ArmorType;
import item.WeaponType;

public class Ranger extends Hero{
    // define starting values of attributes and valid weapon and armor types
    public Ranger(String name) {
        super(name,1,7,1,
                new WeaponType[] {WeaponType.BOW},
                new ArmorType[] {ArmorType.LEATHER,ArmorType.MAIL});
    }

    @Override
    public void increaseAttributes() {
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(5);
        basePrimaryAttributes.increaseIntelligence(1);
        totalPrimaryAttributes.increaseAttributes(new int[] {1,5,1});
    }

    @Override
    public int getTotalMainPrimaryAttribute() {
        return totalPrimaryAttributes.getDexterity();
    }
}
