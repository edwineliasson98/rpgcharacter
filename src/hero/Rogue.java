package hero;

import item.ArmorType;
import item.WeaponType;

public class Rogue extends Hero{
    // define starting values of attributes and valid weapon and armor types
    public Rogue(String name) {
        super(name,2,6,1,
                new WeaponType[] {WeaponType.DAGGER,WeaponType.SWORD},
                new ArmorType[] {ArmorType.LEATHER, ArmorType.MAIL});
    }

    @Override
    public void increaseAttributes() {
        basePrimaryAttributes.increaseStrength(1);
        basePrimaryAttributes.increaseDexterity(4);
        basePrimaryAttributes.increaseIntelligence(1);
        totalPrimaryAttributes.increaseAttributes(new int[] {1,4,1});
    }

    @Override
    public int getTotalMainPrimaryAttribute() {
        return totalPrimaryAttributes.getDexterity();
    }
}
