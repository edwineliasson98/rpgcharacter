import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import hero.*;
import item.*;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Greetings traveler!");
        String name = getNameFromUser(userInput);
        System.out.println("Please select a class, " + name);
        int selection = getClassChoiceFromUser(userInput);
        Hero hero = createHero(name, selection);
        getHeroActionsFromUser(userInput,hero);
    }

    // preset the user with actions that it can perform
    public static void getHeroActionsFromUser(Scanner userInput, Hero hero) {
        String action = "";
        while(!action.equalsIgnoreCase("q") && !action.equalsIgnoreCase("quit")){
            System.out.println("Now then, what would you like to do, " + hero.getName() + " the " + hero.getClass().getSimpleName() + "?");
            System.out.println("(Your options are: 'level', 'equip weapon', 'equip armor', 'stats' and 'quit')");
            action = userInput.nextLine();
            switch (action.toLowerCase()) {
                case "level":
                    hero.levelUp();
                    System.out.println("Leveled up! You are now level " + hero.getLevel() + "!");
                    break;
                case "equip weapon":
                    equipWeapon(hero);
                    System.out.println("Weapon equipped!");
                    break;
                case "equip armor":
                    equipArmor(hero);
                    System.out.println("Armor equipped!");
                    break;
                case "stats":
                    getStats(hero);
                    break;
                case "quit":
                case "q":
                    break;
                default:
                    System.out.println("Invalid input!");
            }
            System.out.println();
        }
    }

    private static void getStats(Hero hero) {
        System.out.println(hero);
    }

    // equips one piece of armor per class type
    private static void equipArmor(Hero hero) {
        switch (hero.getClass().getSimpleName()) {
            case "Mage" -> {
                Armor head = new Armor("Vine Circlet",
                        1,
                        Slot.HEAD,
                        ArmorType.CLOTH,
                        0, 0, 3);
                try {
                    hero.equipArmor(head);
                } catch (InvalidArmorException e) {
                    System.out.println(e.getMessage());
                }
            }
            case "Ranger" -> {
                Armor legs = new Armor("Common Mail Legs",
                        1,
                        Slot.LEGS,
                        ArmorType.MAIL,
                        1, 4, 0);
                try {
                    hero.equipArmor(legs);
                } catch (InvalidArmorException e) {
                    System.out.println(e.getMessage());
                }
            }
            case "Rogue" -> {
                Armor leatherBodyArmor = new Armor("Shabby Jerkin",
                        1,
                        Slot.BODY,
                        ArmorType.LEATHER,
                        1, 6, 2);
                try {
                    hero.equipArmor(leatherBodyArmor);
                } catch (InvalidArmorException e) {
                    System.out.println(e.getMessage());
                }
            }
            case "Warrior" -> {
                Armor plateBodyArmor = new Armor("Plate Vest",
                        1,
                        Slot.BODY,
                        ArmorType.PLATE,
                        6, 3, 0);
                try {
                    hero.equipArmor(plateBodyArmor);
                } catch (InvalidArmorException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    // equips a weapon based on character class
    private static void equipWeapon(Hero hero) {
        switch (hero.getClass().getSimpleName()) {
            case "Mage" -> {
                Weapon wand = new Weapon("Driftwood Wand",
                        1,
                        5,
                        1.0,
                        WeaponType.WAND);
                try {
                    hero.equipWeapon(wand);
                } catch (InvalidWeaponException e) {
                    System.out.println(e.getMessage());
                }
            }
            case "Ranger" -> {
                Weapon bow = new Weapon("Crude Bow",
                        1,
                        7,
                        1.4,
                        WeaponType.BOW);
                try {
                    hero.equipWeapon(bow);
                } catch (InvalidWeaponException e) {
                    System.out.println(e.getMessage());
                }
            }
            case "Rogue" -> {
                Weapon dagger = new Weapon("Glass Shank",
                        1,
                        8,
                        1.5,
                        WeaponType.DAGGER);
                try {
                    hero.equipWeapon(dagger);
                } catch (InvalidWeaponException e) {
                    System.out.println(e.getMessage());
                }
            }
            case "Warrior" -> {
                Weapon sword = new Weapon("Rusted Sword",
                        1,
                        6,
                        1.55,
                        WeaponType.SWORD);
                try {
                    hero.equipWeapon(sword);
                } catch (InvalidWeaponException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    // creates a hero based on selection
    public static Hero createHero(String name, int selection) {
        return switch (selection) {
            case 1 -> new Mage(name);
            case 2 -> new Ranger(name);
            case 3 -> new Rogue(name);
            case 4 -> new Warrior(name);
            default -> null;
        };
    }

    // prompts the user for a class choice
    public static int getClassChoiceFromUser(Scanner userInput) {
        int selection = -1;
        while(selection < 1) {
            System.out.println();
            System.out.println("Choose from these classes:");
            System.out.println("---------------------------");
            System.out.println("1 - Mage, an intelligence based spell caster");
            System.out.println("2 - Ranger, a dexterity based long ranged bow wielder");
            System.out.println("3 - Rogue, a dexterity based close combat dagger wielder");
            System.out.println("4 - Warrior, a strength based close combat berserk");
            try {
                System.out.print("Please choose one of the listed alternatives by entering its respective number: ");
                selection = userInput.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("Invalid choice!");
                selection = -1;
            }
            if (selection > 4) {
                System.out.println("Invalid choice!");
                selection = -1;
            }
            userInput.nextLine();
            System.out.println();
        }
        return selection;
    }

    // prompts the user for a character name
    public static String getNameFromUser(Scanner userInput) {
        String name = "";
        while(name.equals("")){
            System.out.println("What is your name?");
            System.out.print("Type your name here: ");
            name = userInput.nextLine();
            if (name.equals("")){
                System.out.println("That's not a name!");
            }
        }
        return name;
    }
}
