package attributes;

public class PrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public PrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.strength = primaryAttributes.getStrength();
        this.dexterity = primaryAttributes.getDexterity();
        this.intelligence = primaryAttributes.getIntelligence();
    }

    // getter for all attributes returned as an array
    public int[] getAttributes() {
        return new int[] {strength,dexterity,intelligence};
    }

    // takes an array in assumed order of {str,dex,int} and increases individual attributes accordingly
    public void increaseAttributes(int[] attributes) {
        this.strength += attributes[0];
        this.dexterity += attributes[1];
        this.intelligence += attributes[2];
    }

    // takes an array in assumed order of {str,dex,ind} and increases individual attributes accordingly
    public void removeAttributes(int[] attributes) {
        this.strength -= attributes[0];
        this.dexterity -= attributes[1];
        this.intelligence -= attributes[2];
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void increaseStrength(int strength) {
        this.strength += strength;
    }

    public void increaseDexterity(int dexterity) {
        this.dexterity += dexterity;
    }

    public void increaseIntelligence(int intelligence) {
        this.intelligence += intelligence;
    }

    @Override
    public String toString() {
        return "PrimaryAttributes:\n" +
                "strength=" + strength + "\n" +
                "dexterity=" + dexterity + "\n" +
                "intelligence=" + intelligence;
    }
}
