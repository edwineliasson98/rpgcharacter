package hero;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {

    // AAA: Arrange, Act, Assert

    @Test
    public void GetName_Lucas_ShouldPass() {
        // instantiate hero name to use
        String lucas = "Lucas";
        // instantiate one of the hero classes
        Hero hero = new Mage(lucas);
        // get hero name
        String heroName = hero.getName();
        // assert hero name equals Lucas
        assertEquals(lucas, heroName);
    }

    @Test
    public void HeroLevel_LevelWhenCreated_One() {
        // instantiate one of the hero classes
        Hero hero = new Mage("Edwin");
        // get hero level
        int heroLevel = hero.getLevel();
        // assert hero level equals 1
        assertEquals(1,heroLevel);
    }

    @Test
    public void HeroLevel_LevelWhenLevelGained_Two() {
        // instantiate one of the hero classes
        Hero hero = new Warrior("Edwin");
        // level up hero once
        hero.levelUp();
        // get hero level
        int heroLevel = hero.getLevel();
        // assert hero level equals 2
        assertEquals(2, heroLevel);
    }

    @Test
    public void MageAttributes_MageAttributesWhenCreated_OneOneEight() {
        // instantiate mage class
        Hero mage = new Mage("Edwin");
        // instantiate expected value
        int[] expectedAttributes = {1,1,8};
        // get mage attributes
        int[] mageAttributes = mage.getBaseAttributes();
        // assert mage attributes is [1,1,8]
        assertArrayEquals(expectedAttributes, mageAttributes);
    }

    @Test
    public void RangerAttributes_RangerAttributesWhenCreated_OneSevenOne() {
        // instantiate ranger class
        Hero ranger = new Ranger("Edwin");
        // instantiate expected value
        int[] expectedAttributes = {1,7,1};
        // get ranger attributes
        int[] rangerAttributes = ranger.getBaseAttributes();
        // assert ranger attributes is [1,7,1]
        assertArrayEquals(expectedAttributes, rangerAttributes);
    }

    @Test
    public void RogueAttributes_RogueAttributesWhenCreated_OneSevenOne() {
        // instantiate rogue class
        Hero rogue = new Rogue("Edwin");
        // instantiate expected value
        int[] expectedAttributes = {2,6,1};
        // get rogue attributes
        int[] rogueAttributes = rogue.getBaseAttributes();
        // assert rogue attributes is [2,6,1]
        assertArrayEquals(expectedAttributes, rogueAttributes);
    }

    @Test
    public void WarriorAttributes_WarriorAttributesWhenCreated_OneSevenOne() {
        // instantiate warrior class
        Hero warrior = new Warrior("Edwin");
        // instantiate expected value
        int[] expectedAttributes = {5,2,1};
        // get warrior attributes
        int[] warriorAttributes = warrior.getBaseAttributes();
        // assert warrior attributes is [5,2,1]
        assertArrayEquals(expectedAttributes, warriorAttributes);
    }

    @Test
    public void MageAttributes_MageAttributesAtLvlTwo_TwoTwoThirteen() {
        // instantiate mage class
        Hero mage = new Mage("Edwin");
        // level mage up
        mage.levelUp();
        // instantiate expected value
        int[] expectedAttributes = {2,2,13};
        // get mage attributes
        int[] mageAttributes = mage.getBaseAttributes();
        // assert mage attributes is [2,2,13]
        assertArrayEquals(expectedAttributes, mageAttributes);
    }

    @Test
    public void RangerAttributes_RangerAttributesAtLvlTwo_TwoTwelveTwo() {
        // instantiate ranger class
        Hero ranger = new Ranger("Edwin");
        // level ranger up
        ranger.levelUp();
        // instantiate expected value
        int[] expectedAttributes = {2,12,2};
        // get ranger attributes
        int[] rangerAttributes = ranger.getBaseAttributes();
        // assert ranger attributes is [2,12,2]
        assertArrayEquals(expectedAttributes, rangerAttributes);
    }

    @Test
    public void RogueAttributes_RogueAttributesAtLvlTwo_TreeTenTwo() {
        // instantiate rogue class
        Hero rogue = new Rogue("Edwin");
        // level rogue up
        rogue.levelUp();
        // instantiate expected value
        int[] expectedAttributes = {3,10,2};
        // get rogue attributes
        int[] rogueAttributes = rogue.getBaseAttributes();
        // assert rogue attributes is [2,6,1]
        assertArrayEquals(expectedAttributes, rogueAttributes);
    }

    @Test
    public void WarriorAttributes_WarriorAttributesAtLvlTwo_EightFourTwo() {
        // instantiate warrior class
        Hero warrior = new Warrior("Edwin");
        // level warrior up
        warrior.levelUp();
        // instantiate expected value
        int[] expectedAttributes = {8,4,2};
        // get warrior attributes
        int[] warriorAttributes = warrior.getBaseAttributes();
        // assert warrior attributes is [5,2,1]
        assertArrayEquals(expectedAttributes, warriorAttributes);
    }

}