package item;

import hero.Hero;
import hero.Warrior;
import exceptions.InvalidArmorException;
import exceptions.InvalidWeaponException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {
    private Hero testWarrior;
    private Weapon testAxe;
    private Weapon testBow;
    private Armor testPlateBody;
    private Armor testClothHead;

    // run this before each test
    @BeforeEach
    public void init(){
        testWarrior = new Warrior("Edwin");
        testAxe = new Weapon(
                "Common Axe",
                1,
                7,
                1.1,
                WeaponType.AXE
        );
        testBow = new Weapon(
                "Common Bow",
                1,
                12,
                0.8,
                WeaponType.BOW
        );
        testPlateBody = new Armor(
                "Common Plate Body Armor",
                1,
                Slot.BODY,
                ArmorType.PLATE,
                1,0,0
        );
        testClothHead = new Armor(
                "Common Cloth Head Armor",
                1,
                Slot.HEAD,
                ArmorType.CLOTH,
                0,0,5
        );
    }

    @Test
    public void equipWeapon_InvalidLevel_ShouldThrowException() {
        // Arrange
        // set level requirement of bow
        testAxe.setRequiredLevel(2);
        // expected exception message
        String expectedMessage = "Invalid weapon, could not equip item.";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equipWeapon(testBow));
        // Assert
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void equipArmor_InvalidLevel_ShouldThrowException() {
        // Arrange
        // set level requirement of body armor
        testPlateBody.setRequiredLevel(2);
        // expected exception message
        String expectedMessage = "Invalid armor, could not equip item.";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipArmor(testPlateBody));
        // Assert
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void equipWeapon_InvalidType_ShouldThrowException() {
        // Arrange
        // expected exception message
        String expectedMessage = "Invalid weapon, could not equip item.";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> testWarrior.equipWeapon(testBow));
        // Assert
        assertEquals(expectedMessage,exception.getMessage());
    }

    @Test
    public void equipArmor_InvalidType_ShouldThrowException() {
        // Arrange
        // expected exception message
        String expectedMessage = "Invalid armor, could not equip item.";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> testWarrior.equipArmor(testClothHead));
        // Assert
        assertEquals(expectedMessage,exception.getMessage());
    }

    @Test
    public void equipWeapon_ValidInput_ShouldPass() throws InvalidWeaponException {
        // Arrange
        // expected return value
        boolean expectedValue = true;
        //
        boolean actualValue = testWarrior.equipWeapon(testAxe);
        // Assert
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void equipArmor_ValidInput_ShouldPass() throws InvalidArmorException {
        // Arrange
        // expected return value
        boolean expectedValue = true;
        // Act
        boolean actualValue = testWarrior.equipArmor(testPlateBody);
        // Assert
        assertEquals(expectedValue,actualValue);
    }

    @Test
    public void getDPS_NoWeaponEquipped_ShouldPass() {
        // Arrange
        // expected return value
        double expectedValue = 1*(1+(5/100.0));
        // Act
        double actualValue = testWarrior.getDPS();
        // Assert
        assertEquals(expectedValue, actualValue);
    }

    @Test
    public void getDPS_WithValidWeaponEquipped_ShouldPass() throws InvalidWeaponException {
        // Arrange
        // equip valid weapon
        testWarrior.equipWeapon(testAxe);
        // expected return value (weapon dps) * (total main primary stat)
        double expected = (7*1.1)*(1+(5/100.0));
        // Act
        double actual = testWarrior.getDPS();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void getDPS_WithValidWeaponAndArmorEquipped_ShouldPass() throws InvalidWeaponException,InvalidArmorException {
        // Arrange
        // equip valid weapon
        testWarrior.equipWeapon(testAxe);
        // equip valid armor
        testWarrior.equipArmor(testPlateBody);
        // expected return value (weapon dps) * (total main primary stat)
        double expected = (7*1.1) * (1+((5+1) /100.0));
        // Act
        double actual = testWarrior.getDPS();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void getTotalPrimaryAttribute_WithReplacingValidArmor_ShouldPass() throws InvalidArmorException {
        // Arrange
        // equip valid armor
        testWarrior.equipArmor(testPlateBody);
        // replace with other valid armor
        Armor testMailArmor = new Armor("Common Mail Armor",
                1,
                Slot.BODY,
                ArmorType.MAIL,
                2,1,0);
        testWarrior.equipArmor(testMailArmor);
        // expected return value 7
        int expected = 7;
        // Act
        int actual = testWarrior.getTotalMainPrimaryAttribute();
        // Assert
        assertEquals(expected,actual);
    }
}