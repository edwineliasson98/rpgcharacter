# Java Console Application
### Noroff accelerated - Assignment 5

This is a java console application following the specifications from Assignment 5 in the Noroff accelerated course.

## Prerequisites

You need to have [Java](https://www.oracle.com/java/technologies/downloads/) installed.

## Install

This project uses Java, go check out the [Intellij IDE](https://www.jetbrains.com/idea/) or any similar IDE's to run the program.

## Usage

Open the project in your IDE of choice and run the main file.

## Testing Assumptions

Some methods are not being tested with the assumption that it is not needed. These are simple getters for private attributes as well as the abstract method getTotalMainPrimaryAttribute() for each of the subclasses to hero. This method is tested for the Warrior class, and should work in a very similar fashion but with different primary attribute.

Main isn't tested either as this is mostly user input (which I'm not sure we should test with unit tests).

## Maintainers

[Edwin Eliasson @(edwineliasson98)](https://gitlab.com/edwineliasson98)

## Contributing

PRs accepted.

## License

MIT © Edwin Eliasson
